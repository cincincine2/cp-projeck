﻿using odiProject.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace odiProject.Models
{
    public class CustomerModel
    {
        public customer DefineName(CustomerViewModels customer)
        {
            customer newCustomer = new customer();
            newCustomer.first_name = customer.FirstName;
            newCustomer.last_name = customer.LastName;
            newCustomer.customer_id = customer.CustomerId;
            newCustomer.address = customer.Address;
            newCustomer.city = customer.City;
            newCustomer.state = customer.State;
            newCustomer.zip_code = customer.ZipCode;

            return newCustomer;
        }
        public void Create(CustomerViewModels customer)
        {
            var newCustomers = DefineName(customer);


            using (DbModels dbModels = new DbModels())
            {
                dbModels.customers.Add(newCustomers);
                dbModels.SaveChanges();
            }
            
        }
    }
}