﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace odiProject.ViewModels
{
    public class CustomerViewModels
    {
        [DisplayName("Customer ID")]
        [Required]
        public int CustomerId { get; set; }

        [DisplayName("First Name")]
        [Required]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        [Required]
        public string LastName { get; set; }

        [DisplayName("Address")]
        [Required]
        public string Address{ get; set; }

        [DisplayName("City")]
        [Required]
        public string City { get; set; }

        [DisplayName("State")]
        [Required]
        public string State { get; set; }

        [DisplayName("Zip Code")]
        [Required]
        public string ZipCode { get; set; }
    }


}